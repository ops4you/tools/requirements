#!/usr/bin/env bash
PURPOSE="fetch/install project requirements"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# -- auto: common helper functions
isContainer() { grep -q "/docker" /proc/1/cgroup 2>/dev/null || grep -q "=lxc" /proc/1/environ; }
printEnvs()   { local ENV_EXCEPTIONS="(^LS_|^XDG_|^SSH_|=;)"; env|grep -vE "${ENV_EXCEPTIONS}"|sort; }
printTitle()  { echo;echo; local msg="${*}"; linesno=$(( ${#msg} + 4 )); msglines=$(for i in `seq ${linesno}`;do echo -n "=";done);echo -e "${msglines}\n# ${msg} #\n${msglines}"; }
DEBUG() { local msg="$(date +%F_%T) [${scriptBaseName}] DEBUG ${*}"; [[ "true" = "${VERBOSE}" ]] && echo -e "${msg}" || true; }
PRINT() { local msg="$(date +%F_%T) [${scriptBaseName}] INFO  ${*}"; echo -e "${msg}"; }
ERROR() { local msg="$(date +%F_%T) [${scriptBaseName}] ERROR ${*}"; echo -e "${msg}"; exit ${RC:-1}; }
installOsPackages(){
  local OS_PACKAGES="${*}"
    if apk --version >/dev/null 2>/dev/null;then      PRINT "using apk package manager";  apk update && apk add ${OS_PACKAGES};
  elif apt --version >/dev/null 2>/dev/null;then      PRINT "using apt package manager";  export DEBIAN_FRONTEND=noninteractive; apt-get update -qq && apt install -y ${OS_PACKAGES};
  elif dnf --version >/dev/null 2>/dev/null;then      PRINT "using dnf package manager";  dnf install -y ${OS_PACKAGES};
  elif yum --version >/dev/null 2>/dev/null;then      PRINT "using yum package manager";  yum install -y ${OS_PACKAGES};
  elif microdnf -h   >/dev/null 2>/dev/null;then      PRINT "using µdnf package manager"; microdnf install -y ${OS_PACKAGES};
  else ERROR "no supported package manager found";fi
}
# -- /auto: common helper functions


# -- script usage
USAGE() {
  printTitle "USAGE"
  echo "PURPOSE: ${PURPOSE}"
  echo "SYNTAX:  ${scriptName} [OPTIONS] <TYPE1> [[TYPE2] ... [TYPEn]]"
  printTitle "SUPPORTED TYPES"
  echo "all                       setup all available requirements"
  echo "ext                       download defined external tools"
  echo "get                       download defined external files (generic)"
  echo "git                       download defined external git repos"
  echo "img                       download defined docker images"
  echo "pip                       install  defined pip packages"
  echo "pkg                       install  defined os packages"
  echo "xgz                       download and extract defined external archives (tgz|zip)"
  printTitle "OPTIONS"
  echo "-h|--help                 print out this help"
  echo "-p|--dst-path PATH        set base path for extraction tasks (ext,get,git,img,tgz) DEFAULT=${PWD}"
  echo "-t|--tmp-path PATH        set temp path for extraction tasks (ext,get,git,img,tgz) DEFAULT=${PWD}/tmp"
  echo "-v|--verbose              enable verbose output (debugging infos)"
  echo
  [[ -z "${1}" ]] || exit ${1}
}
# -- /script usage


# -- args parsing
# reset params & set default settings
params=""
dst_path="${PWD}"
req_types=""
# no params
[[ -n "${1}" ]] || NOARGS=true
# other params
while [[ -n "${1}" ]]
do
  case "${1}" in
    -h|--help|help)
      HELP=true
    ;;
    -p|--dst-path)
      shift
      dst_path="${1}"
    ;;
    -t|--tmp-path)
      shift
      tmp_path="${1}"
    ;;
    -v|--verbose)
      VERBOSE=true
    ;;
    all)
      install_all=true
    ;;
    ext|get|git|img|pkg|pip|xgz)
      req_types+="${1} "
    ;;
    *)
      params+="${1} "
    ;;
  esac
  shift
done
# -- /args parsing


# -- args checking
# print usage
if [[ "true" = "${HELP}" ]]
then
  USAGE 0
fi
# invalid params
if [[ -n "${params}" ]]
then
  USAGE
  ERROR "invalid parameter(s): ${params}"
fi
# use default type if none provided
if [[ -z "${req_types}" ]]
then
  req_types="git"
  DEBUG "using default type(s): ${req_types}"
fi
# default tmp_path
[[ -n "${tmp_path}" ]] || tmp_path="${dst_path}/tmp"
# -- /args checking


# ENABLE ERROR HANDLING
# =====================

# exit on any error
set -e

# trap error events
report_errors() { echo -e "\033[31m[\033[33m${scriptName}\033[31m] ERROR occured on line [\033[33m${1}\033[31m]\033[0m"; }
trap 'report_errors ${LINENO}' ERR

# switch to workspace
[[ -n "${WORKSPACE}" ]] || WORKSPACE=${PWD}
cd ${WORKSPACE} || false
PRINT "starting script from WORKSPACE: ${PWD}"


# SETUP MY OWN DEPENDENCIES
# =========================
printTitle "CHECKING OS-SPECIFIC REQUIREMENTS"

# DEFAULTS ---------------------------------------------------------------------
  SYS_NAME="UNSUPPORTED"
# RHEL7 ------------------------------------------------------------------------
if grep -qE "release 7" /etc/redhat-release 2>/dev/null
then
  SYS_NAME="RHEL7"
  PYTHON_VERSION_SUFFIX=""
  PRINT "${SYS_NAME} system detected: using python${PYTHON_VERSION_SUFFIX}"
  PRINT "${SYS_NAME} system detected: install specific os packages"
  installOsPackages epel-release
# RHEL8 ------------------------------------------------------------------------
elif grep -qE "release 8" /etc/redhat-release 2>/dev/null
then
  SYS_NAME="RHEL8"
  PYTHON_VERSION_SUFFIX="3"
  PRINT "${SYS_NAME} system detected: using python${PYTHON_VERSION_SUFFIX}"
  PRINT "${SYS_NAME} system detected: install specific os packages"
  installOsPackages epel-release
# ALP3X ------------------------------------------------------------------------
elif grep -qE "3\." /etc/alpine-release 2>/dev/null
then
  SYS_NAME="ALP3X"
  PYTHON_VERSION_SUFFIX="3"
  PRINT "${SYS_NAME} system detected: using python${PYTHON_VERSION_SUFFIX}"
  PRINT "${SYS_NAME} system detected: install specific os packages"
  installOsPackages libc6-compat openssh-keygen python3 py3-cryptography
# LTS18 ------------------------------------------------------------------------
elif grep -qE "18.04" /etc/os-release 2>/dev/null
then
  SYS_NAME="LTS18"
  PYTHON_VERSION_SUFFIX=""
  PRINT "${SYS_NAME} system detected: using python${PYTHON_VERSION_SUFFIX}"
  PRINT "${SYS_NAME} system detected: install specific os packages"
  installOsPackages curl gnupg
  # add libcontainers repo
  . /etc/os-release
  sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${NAME}_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
  curl -kL https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/x${NAME}_${VERSION_ID}/Release.key | apt-key add -
  apt-get update -qq
# LTS18 ------------------------------------------------------------------------
elif grep -qE "20.04" /etc/os-release 2>/dev/null
then
  SYS_NAME="LTS20"
  PYTHON_VERSION_SUFFIX="3"
  PRINT "${SYS_NAME} system detected: using python${PYTHON_VERSION_SUFFIX}"
  PRINT "${SYS_NAME} system detected: install specific os packages"
  installOsPackages curl gnupg
  # add libcontainers repo
  . /etc/os-release
  sh -c "echo 'deb http://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/x${NAME}_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
  curl -kL https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/x${NAME}_${VERSION_ID}/Release.key | apt-key add -
  apt-get update -qq
fi


printTitle "CHECKING REQUIREMENTS: [pip${PYTHON_VERSION_SUFFIX}]"
# install pip if not present yet
if ! pip${PYTHON_VERSION_SUFFIX} --version >/dev/null 2>/dev/null
then
  PRINT "trying to install missing requirements: pip${PYTHON_VERSION_SUFFIX}"
  if [[ "ALP3X" = "${SYS_NAME}" ]];then
    installOsPackages py${PYTHON_VERSION_SUFFIX}-pip
  else
    installOsPackages python${PYTHON_VERSION_SUFFIX}-pip
  fi
fi
# update pip
PRINT "updating pip${PYTHON_VERSION_SUFFIX}"
if [[ -z "${PYTHON_VERSION_SUFFIX}" ]];then
  pip${PYTHON_VERSION_SUFFIX} install --upgrade 'pip<21'
else
  pip${PYTHON_VERSION_SUFFIX} install --upgrade pip
fi
if [[ ! -e /usr/bin/pip ]]
then
  PRINT "creating symlink /usr/bin/pip => /usr/bin/pip3"
  ln -s /usr/bin/pip3 /usr/bin/pip
fi

printTitle "CHECKING REQUIREMENTS: [ansible-galaxy]"
if ! ansible-galaxy --version >/dev/null 2>/dev/null
then
  PRINT "trying to install missing requirements: ansible (required for ansible-galaxy)"
  pip${PYTHON_VERSION_SUFFIX} install ansible
fi

printTitle "CHECKING REQUIREMENTS: [curl]"
if ! curl --version >/dev/null 2>/dev/null
then
  PRINT "trying to install missing requirements: curl"
  installOsPackages curl
fi

printTitle "CHECKING REQUIREMENTS: [gettext]"
if ! envsubst --version >/dev/null 2>/dev/null
then
  PRINT "trying to install missing requirements: gettext (required for envsubst)"
  installOsPackages gettext
fi

printTitle "CHECKING REQUIREMENTS: [git]"
if ! git --version >/dev/null 2>/dev/null
then
  PRINT "trying to install missing requirements: git"
  installOsPackages git
fi

printTitle "CHECKING REQUIREMENTS: [skopeo]"
if ! skopeo --version >/dev/null 2>/dev/null
then
  PRINT "trying to install missing requirements: skopeo"
  installOsPackages skopeo
fi

printTitle "CHECKING REQUIREMENTS: [unzip]"
if ! unzip -h >/dev/null 2>/dev/null
then
  PRINT "trying to install missing requirements: unzip"
  installOsPackages unzip
fi

printTitle "CHECKING REQUIREMENTS: [arkade]"
if [[ -f ${scriptFullDir}/requirements.ark ]] && ! arkade -h >/dev/null 2>/dev/null
then
  PRINT "trying to install missing requirements: arkade"
  curl -sLS https://get.arkade.dev | sh
fi


# SETUP RUNTIME ENVIRONMENT
# =========================

# include local development files if exists
local_environment_file=${scriptFullDir}/requirements.dev
if [[ -f ${local_environment_file} ]]
then
  PRINT "importing local_environment_file: ${local_environment_file}"
  set -o allexport
  source ${local_environment_file}
  set +o allexport
fi

# include ansible environment file if exists
ansible_environment_file=${scriptFullDir}/ansible.env
if [[ -f ${ansible_environment_file} ]]
then
  PRINT "importing ansible_environment_file: ${ansible_environment_file}"
  set -o allexport
  source ${ansible_environment_file}
  set +o allexport
fi

# setup key for ssh connections
ansible_key_file=${scriptFullDir}/ansible.key
ansible_pub_file=${scriptFullDir}/ansible.pub
# create file if not exists
if [[ ! -f ${ansible_key_file} ]]
then
  PRINT "creating ssh key: ${ansible_key_file}"
  ssh-keygen -C "ansible-key@$(basename ${scriptFullDir})" -f ${ansible_key_file} -q -N ""
  mv ${ansible_key_file}.pub ${ansible_pub_file}
fi
# set file permissions
chmod 600 ${ansible_key_file}




# SETUP PROJECT DEPENDENCIES
# ==========================

# show debug infos
if [[ "true" = "${VERBOSE}" ]]
then
  printTitle "CURRENT ENVIRONMENT"
  printEnvs
  printTitle "CURRENT SETTINGS"
cat << tac
  params: ${params}
  requirements types: ${req_types}
  extraction dst_path: ${dst_path}
  extraction tmp_path: ${tmp_path}
tac
fi

# setup dst_path
if [[ ! -d ${dst_path} ]]
then
  DEBUG "creating dst_path: ${dst_path}"
  mkdir -p ${dst_path}
fi

# setup tmp_path
if [[ ! -d ${tmp_path} ]]
then
  DEBUG "creating tmp_path: ${tmp_path}"
  mkdir -p ${tmp_path}
fi

# switch to dst_path
pushd ${dst_path} >/dev/null


# installing os packages first
this_type="pkg"
if [[ "${req_types}" =~ ${this_type} ]] || [[ "true" = "${install_all}" ]]
then
  this_requirement_file="${scriptFullDir}/requirements.${this_type}"
  if [[ -f ${this_requirement_file} ]]
  then
    printTitle "${this_type} dependencies"
    DEBUG "processing requirement file: ${this_requirement_file}"
    DEBUG "replacing env vars from requirement file"
    envsubst <${this_requirement_file} >${tmp_path}/requirements_parsed.${this_type}
    OS_PACKAGES=$(grep -vE "^(#.*|$)"   ${tmp_path}/requirements_parsed.${this_type} || true)
    if [[ -n "${OS_PACKAGES}" ]]
    then
      PRINT "installing ${this_type} dependencies"
      DEBUG "OS_PACKAGES: " $(tr " " ';' <<< "${OS_PACKAGES}")
      installOsPackages ${OS_PACKAGES}
    fi
  fi
fi


# installing pip packages
this_type="pip"
if [[ "${req_types}" =~ ${this_type} ]] || [[ "true" = "${install_all}" ]]
then
  this_requirement_file="${scriptFullDir}/requirements.${this_type}"
  # try also requirements.txt for compatibility with other projects
  [[ -f ${this_requirement_file} ]] || this_requirement_file="${scriptFullDir}/requirements.txt"
  # continue only if a requirements file is present
  if [[ -f ${this_requirement_file} ]]
  then
    printTitle "${this_type} dependencies"
    DEBUG "processing requirement file: ${this_requirement_file}"
    DEBUG "replacing env vars from requirement file"
    envsubst <${this_requirement_file} >${tmp_path}/requirements_parsed.${this_type}
    PIP_PACKAGES=$(grep -vE "^(#.*|$)"  ${tmp_path}/requirements_parsed.${this_type} || true)
    if [[ -n "${PIP_PACKAGES}" ]]
    then
      PRINT "installing ${this_type} dependencies"
      DEBUG "PIP_PACKAGES: " $(tr " " ';' <<< "${PIP_PACKAGES}")
      pip${PYTHON_VERSION_SUFFIX} install -r ${tmp_path}/requirements_parsed.${this_type}
    fi
  fi
fi


# fetching external tools
this_type="ext"
if [[ "${req_types}" =~ ${this_type} ]] || [[ "true" = "${install_all}" ]]
then
  this_requirement_file="${scriptFullDir}/requirements.${this_type}"
  if [[ -f ${this_requirement_file} ]]
  then
    printTitle "${this_type} dependencies"
    DEBUG "processing requirement file: ${this_requirement_file}"
    DEBUG "replacing env vars from requirement file"
    envsubst <${this_requirement_file} >${tmp_path}/requirements_parsed.${this_type}
    for TOOL in $(grep -vE "^(#.*|$)"   ${tmp_path}/requirements_parsed.${this_type} || true)
    do
      tool_download_script=${scriptFullDir}/tools.download.${TOOL}
      if [[ -f ${tool_download_script} ]]
      then
        PRINT "fetching external tool: ${TOOL}"
        bash ${tool_download_script}
      else
        ERROR "missing external tool download script: ${tool_download_script}"
      fi
    done
  fi
fi


# fetching external files
this_type="get"
if [[ "${req_types}" =~ ${this_type} ]] || [[ "true" = "${install_all}" ]]
then
  this_requirement_file="${scriptFullDir}/requirements.${this_type}"
  if [[ -f ${this_requirement_file} ]]
  then
    printTitle "${this_type} dependencies"
    DEBUG "processing requirement file: ${this_requirement_file}"
    DEBUG "replacing env vars from requirement file"
    envsubst <${this_requirement_file} >${tmp_path}/requirements_parsed.${this_type}
    for URL in $(grep -vE "^(#.*|$)"    ${tmp_path}/requirements_parsed.${this_type} || true)
    do
      PRINT "[>] fetching external file from url: ${URL}"
      curl -kLO "${URL}"
    done
  fi
fi


# fetching git repositories
this_type="git"
if [[ "${req_types}" =~ ${this_type} ]] || [[ "true" = "${install_all}" ]]
then
  this_requirement_file="${scriptFullDir}/requirements.${this_type}"
  if [[ -f ${this_requirement_file} ]]
  then
    printTitle "${this_type} dependencies"
    DEBUG "processing requirement file: ${this_requirement_file}"
    DEBUG "replacing env vars from requirement file"
    envsubst <${this_requirement_file}       >${tmp_path}/requirements_parsed.yml
    GIT_DEFINITIONS=$(grep -vE "^(#.*|---|$)" ${tmp_path}/requirements_parsed.yml || true)
    if grep -q "[a-z]" <<<"${GIT_DEFINITIONS}"
    then
      PRINT "fetching ${this_type} dependencies into ${dst_path}"
      ansible-galaxy install --force -r ${tmp_path}/requirements_parsed.yml --roles-path ${dst_path}
    else
      PRINT "skipping empty ${this_type} definitions"
    fi
  fi
fi


# fetching docker images
this_type="img"
if [[ "${req_types}" =~ ${this_type} ]] || [[ "true" = "${install_all}" ]]
then
  this_requirement_file="${scriptFullDir}/requirements.${this_type}"
  if [[ -f ${this_requirement_file} ]]
  then
    printTitle "${this_type} dependencies"
    DEBUG "processing requirement file: ${this_requirement_file}"
    DEBUG "replacing env vars from requirement file"
    envsubst <${this_requirement_file} >${tmp_path}/requirements_parsed.${this_type}
    for IMG in $(grep -vE "^(#.*|$)"    ${tmp_path}/requirements_parsed.${this_type})
    do
      old_tag="$(basename ${IMG})"
      new_tag="${old_tag/:*/:stable}"
      DEBUG "renaming tag (${old_tag} => ${new_tag})"
      img_basename="${new_tag/:/_}"
      dst_basename="${img_basename}.tgz"
      if [[ -f ${dst_path}/${dst_basename} ]]
      then
        DEBUG "using existing export file ${dst_path}/${dst_basename}"
      else
        DEBUG "exporting docker image [${IMG}] to archive ${dst_path}/${img_basename}"
        skopeo copy --remove-signatures docker://${IMG} docker-archive://${tmp_path}/${img_basename}.tar
        DEBUG "extracting tar archive to ${tmp_path}/${img_basename}"
        rm -rf ${tmp_path}/${img_basename} || true
        mkdir -p ${tmp_path}/${img_basename}
        tar -C ${tmp_path}/${img_basename} -xvf ${tmp_path}/${img_basename}.tar
        DEBUG "replacing repoTags values in ${tmp_path}/${img_basename}/manifest.json"
        sed -i "s#\(\"RepoTags\":\)[^,]*,#\1[\"${new_tag}\"],#" ${tmp_path}/${img_basename}/manifest.json
        cat ${tmp_path}/${img_basename}/manifest.json
        PRINT "creating compressed archive: ${dst_path}/${dst_basename}"
        tar -C ${tmp_path}/${img_basename} -cvzf ${dst_path}/${dst_basename} .
        ls -lah ${dst_path}/${dst_basename}
      fi
    done
  fi
fi


# fetching & extracting external archives
this_type="xgz"
if [[ "${req_types}" =~ ${this_type} ]] || [[ "true" = "${install_all}" ]]
then
  this_requirement_file="${scriptFullDir}/requirements.${this_type}"
  if [[ -f ${this_requirement_file} ]]
  then
    printTitle "${this_type} dependencies"
    DEBUG "processing requirement file: ${this_requirement_file}"
    DEBUG "replacing env vars from requirement file"
    envsubst <${this_requirement_file} >${tmp_path}/requirements_parsed.${this_type}
    DEBUG "downloading remote archives first"
    archives_remote=""
    for URL in $(grep -E "^(f|h)ttp.*:\/\/.*\.(tar.gz|tgz|zip)" ${tmp_path}/requirements_parsed.${this_type} || true)
    do
      archive_base=$(basename ${URL})
      archive_name=${archive_base%%.*}
      archive_type=${archive_base#*.}
      # homogenizing types
      if [[ "${archive_type}" =~ tar\.gz ]];then
        archive_type=tgz
      elif [[ "${archive_type}" =~ \.gz ]];then
        archive_type=zip
      fi
      PRINT "[>] fetching external ${archive_type}-file from url: ${URL}"
      curl -kL -o ${tmp_path}/${archive_name}.${archive_type} "${URL}"
      archives_remote+="${tmp_path}/${archive_name}.${archive_type} "
    done
    DEBUG "archives_remote: " $(tr " " ';' <<< "${archives_remote}")
    archives_local=$(grep -vE "^(#.*|(f|h)ttp.*:\/\/|$)" ${tmp_path}/requirements_parsed.${this_type} || true)
    DEBUG "archives_local: " $(tr " " ';' <<< "${archives_local}")
    archives_merged="${archives_remote} ${archives_local}"
    if [[ -n "${archives_merged}" ]]
    then
      PRINT "installing ${this_type} dependencies"
      for ARCHIVE in $(find ${tmp_path} | grep -E ".*(tgz|zip)")
      do
        archive_type=${ARCHIVE#*.}
        case "${archive_type}" in
          tgz)
            PRINT "extracting archive ${ARCHIVE} to ${dst_path}"
            [[ "true" = ${VERBOSE} ]] && VERBOSE_OPTIONS="-v" || VERBOSE_OPTIONS=""
            tar -C ${dst_path} ${VERBOSE_OPTIONS} -xzf ${ARCHIVE}
          ;;
          zip)
            PRINT "extracting archive ${ARCHIVE} to ${dst_path}"
            [[ "true" = ${VERBOSE} ]] && VERBOSE_OPTIONS="" || VERBOSE_OPTIONS="-q"
            unzip ${VERBOSE_OPTIONS} -o ${ARCHIVE} -d ${dst_path}
          ;;
          *)
            ERROR "unsupported archive type: ${archive_type}"
          ;;
        esac
      done
    fi
  fi
fi


# fetching arkade cli tools
this_type="ark"
if [[ "${req_types}" =~ ${this_type} ]] || [[ "true" = "${install_all}" ]]
then
  this_requirement_file="${scriptFullDir}/requirements.${this_type}"
  if [[ -f ${this_requirement_file} ]]
  then
    printTitle "${this_type} dependencies"
    DEBUG "processing requirement file: ${this_requirement_file}"
    DEBUG "replacing env vars from requirement file"
    envsubst <${this_requirement_file} >${tmp_path}/requirements_parsed.${this_type}
    for APP in $(grep -vE "^(#.*|$)"    ${tmp_path}/requirements_parsed.${this_type} || true)
    do
      PRINT "[>] fetching arkade tool: ${APP}"
      arkade get ${APP} && mv -v ${HOME}/.arkade/bin/* ${dst_path}
    done
  fi
fi

# switch back to previous path
popd >/dev/null

# eof
